git config --global user.name "Ronald Perez Alvarez"
git config --global user.email "rperezalvarez@gmail.com"

Crear un nuevo repositorio

git clone https://gitlab.com/rperezalvarez/bienvenida.git
cd bienvenida
git switch -c main
touch README.md
git add README.md
git commit -m "add README"

Push a una carpeta existente

cd existing_folder
git init --initial-branch=main
git remote add origin https://gitlab.com/rperezalvarez/bienvenida.git
git add .
git commit -m "Initial commit"

Push a un repositorio de Git existente

cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/rperezalvarez/bienvenida.git

